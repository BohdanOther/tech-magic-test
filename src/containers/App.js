import React, { Component } from 'react'

import api from '../api/fixture-api'
import EnchancedPostList from './EnchancedPostList'

import '../styles/App.css'

export default class App extends Component {
  state = {
    cities: [],
    companies: [],
    posts: []
  }

  componentDidMount() {
    // simulate api request
    const posts = api.getPosts();
    const users = api.getUsers();

    // faster user access by userId
    const userMap = new Map(users.map(u => [Number(u.id), u]));

    // could keep data normalized and pass both users and posts
    // but let's make our life simpler
    const mappedPosts = posts.map(post => {
      const user = userMap.get(post.userId);

      return {
        id: post.id,
        title: post.title,
        body: post.body,
        author: {
          name: user.name,
          city: user.address.city,
          company: user.company.name,
        }
      }
    });

    this.setState({
      posts: mappedPosts,
      cities: [...new Set(users.map(u => u.address.city))], // exclude city duplicated
      companies: [...new Set(users.map(u => u.company.name))] // exclude company duplicates
    })
  }

  handlePostDelete = (postId) => {
    this.setState((prevState) => {
      return {
        posts: prevState.posts.filter(p => p.id !== postId)
      };
    });

    // TODO: can also recompute cities and companies 
    // since we're removing posts
  }

  render() {
    return (
      <div>
        <h1>Posts</h1>
        <EnchancedPostList
          {...this.state}
          onPostDelete={this.handlePostDelete}
        />
      </div>
    );
  }
}