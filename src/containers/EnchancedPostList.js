import React, { Component, PropTypes } from 'react'

import PostList from '../components/PostList'
import PostFilter from '../components/PostFilter'

export default class EnchancedPostList extends Component {
    state = {
        titleFilter: '',
        selectedCity: '',
        selectedCompany: '',
        selectedSortCriteria: ''
    }

    static defaultProps = {
        posts: [],
        cities: [],
        companies: [],
        onPostDelete: () => { }
    }

    static propTypes = {
        posts: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            body: PropTypes.string,
            author: PropTypes.shape({
                name: PropTypes.string,
                city: PropTypes.string,
                company: PropTypes.string
            })
        })).isRequired,
        cities: PropTypes.arrayOf(PropTypes.string),
        companies: PropTypes.arrayOf(PropTypes.string)
    }

    getFilteredPosts(posts) {
        const {
            titleFilter,
            selectedCity,
            selectedCompany
        } = this.state;

        return posts.filter(post =>
            (!titleFilter || post.title.toLowerCase().indexOf(titleFilter.toLowerCase()) !== -1)
            && (!selectedCity || post.author.city === selectedCity)
            && (!selectedCompany || post.author.company === selectedCompany)
        );
    }

    getSortedPosts(posts) {
        const { selectedSortCriteria } = this.state;

        // TODO: Chance for optimization here:
        // can skip redundant sorts
        // when user city (company, author) filter is selected
        switch (selectedSortCriteria) {
            case 'author':
                return posts.sort((a, b) =>
                    a.author.name.localeCompare(
                        b.author.name));

            case 'city':
                return posts.sort((a, b) =>
                    a.author.city.localeCompare(
                        b.author.city));

            case 'company':
                return posts.sort((a, b) =>
                    a.author.company.localeCompare(
                        b.author.company));

            default:
                return posts;
        }
    }

    handleFilterChange = ({ target }) => {
        const { name, value } = target;
        this.setState({
            [name]: value
        })
    }

    render() {
        const {
            selectedSortCriteria,
            selectedCity,
            selectedCompany,
            titleFilter
        } = this.state;

        const {
            posts,
            cities,
            companies
        } = this.props;

        let postsToRender = this.getFilteredPosts(posts);
        postsToRender = this.getSortedPosts(postsToRender);

        return (
            <div>
                <PostFilter
                    selectedCity={selectedCity}
                    selectedCompany={selectedCompany}
                    selectedSortCriteria={selectedSortCriteria}

                    availableCities={cities}
                    availableCompanies={companies}
                    availableSortCriterias={['author', 'city', 'company']}

                    titleFilter={titleFilter}

                    onParameterChange={this.handleFilterChange}
                />

                <PostList
                    posts={postsToRender}
                    onDelete={this.props.onPostDelete}
                />
            </div>
        );
    }
}