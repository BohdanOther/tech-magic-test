export default {

  getPosts: () => {
    return require('../fixtures/posts.json');
  },

  getUsers: () => {
    return require('../fixtures/users.json');
  },

  getUser: (id) => {
    return require('../fixtures/users.json').find(user => Number(user.id) === id) || {};
  }

}
