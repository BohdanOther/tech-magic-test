import React from 'react'

export default function PostFilterInput({
    label = '',
    options,
    optionPlaceholder = '',
    ...props
}) {
    const input = options
        ? (
            <select {...props}>
                <option value=''>{optionPlaceholder}</option>
                {
                    options.map(opt =>
                        <option key={opt} value={opt}>{opt}</option>
                    )
                }
            </select>
        )
        : (
            <input {...props} />
        );

    return (
        <label>
            {label}:
            {input}
        </label>
    );
}