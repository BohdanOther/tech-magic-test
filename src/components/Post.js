import React from 'react'

import capitalize from '../utils/capitalize'

export default function Post({ id, title, body, author, company, city, onDelete }) {
    return (
        <article>
            <h2>{capitalize(title)}</h2>
            <span>{author}</span> - <span>{company}</span> - <span>{city}</span>
            <p>{capitalize(body)}</p>
            {/* it's ok to pass lambda here, but kind of bug prone */}
            <button onClick={() => onDelete(id)}>X</button>
        </article>
    );
}

Post.defaultProps = {
    title: '',
    body: '',
    author: '',
    company: '',
    city: '',
    onDelete: () => { }
}