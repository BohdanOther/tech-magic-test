import React from 'react'

import Post from './Post'

export default function PostList({ posts, onDelete }) {
    const postList = posts.map(post =>
        <Post
            key={post.id}
            id={post.id}
            title={post.title}
            body={post.body}
            author={post.author.name}
            company={post.author.company}
            city={post.author.city}
            onDelete={onDelete}
        />
    );

    return (
        <div>{postList}</div>
    );
}