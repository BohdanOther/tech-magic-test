import React from 'react'

import PostFilterInput from './PostFilterInput'

export default function PostFilter({
    availableCities,
    availableCompanies,
    availableSortCriterias,

    selectedCity,
    selectedCompany,
    selectedSortCriteria,

    titleFilter,

    onParameterChange
 }) {
    return (
        <nav>
            <PostFilterInput
                label="City filter"
                options={availableCities}
                optionPlaceholder="Select city"
                name="selectedCity"
                value={selectedCity}
                onChange={onParameterChange}
            />

            <PostFilterInput
                label="Company filter"
                options={availableCompanies}
                optionPlaceholder="Select company"
                name="selectedCompany"
                value={selectedCompany}
                onChange={onParameterChange}
            />

            <PostFilterInput
                label="Quick search by post title"
                type="text"
                name="titleFilter"
                value={titleFilter}
                onChange={onParameterChange}
            />

            <hr />

            <PostFilterInput
                label="Sort by"
                options={availableSortCriterias}
                optionPlaceholder="Sort by"
                name="selectedSortCriteria"
                value={selectedSortCriteria}
                onChange={onParameterChange}
            />
        </nav>
    );
}

PostFilter.defaultProps = {
    availableCities: [],
    availableCompanies: [],
    availableSortCriterias: [],

    selectedCity: null,
    selectedCompany: null,
    selectedSortCriteria: null,

    titleFilter: '',

    onParameterChange: () => { }
}